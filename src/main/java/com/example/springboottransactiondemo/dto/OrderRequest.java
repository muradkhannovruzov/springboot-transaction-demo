package com.example.springboottransactiondemo.dto;

import com.example.springboottransactiondemo.entity.Order;
import com.example.springboottransactiondemo.entity.Payment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderRequest {
    private Order order;
    private Payment payment;
}
